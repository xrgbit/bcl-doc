#+title: Unicode
#+roam_tags: character
* Unicode 15.1.0

Supports up to 1,114,112 code points, which only requires at least 21 bits. Two can fit
comfortably in a 64 bit tag bit

** triple character
For case folding, the maximum number of codes that would be needed are 3. However, the max
value of one of the folded characters isn't going to need the max 21, so we could kludge
things

#+begin_src lisp :dir ".."
  (load "testing.lisp")
  (in-package #:playground)
  (log (loop :for i :in (reduce #'append
                                (mapcar (s:op (third _))
                                        (parse-case-folding-database)))
             :maximize i)
       2)
#+end_src

#+RESULTS:
: 16.934464

