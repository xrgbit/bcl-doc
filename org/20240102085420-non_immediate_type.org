#+title: non-immediate type

Non-Immediate types make up every object that is not an [[file:20240101112501-immediate_type.org][immediate type]].

Every lisp object:
- Contains a lisp header (the same size as a lisp object pointer), which encodes
  information about the object: its type (if it's actually a forwarded pointer), its size,
  space for flags
  - =[... 1]= Where the lowest bit is 1, and when zeroed represent a lisp object tagged
    pointer to where the object actually resides. This is used when the object has been
    garbage collected and has been moved to another location.
    - Note here we are taking advantage that our object are aligned, due to how we encode
      the [[file:20240101112501-immediate_type.org][immediate types]], since the actual pointer is guaranteed to be aligned by at
      least 4 bytes.
    - We use the header which used to encode more information about the object, and treat
      it as a pointer to a new location. Note that the "object" is still followed by this
      header, but that it's not longer meaning full to refer to the object by the address
      used to get to the object. The object now lives where it's pointed to.
  - =[ssss.ssss ssss.ssss ssss.ssss ssss.ssss - ssss.ssss ssss.ssss ssss.ssss tttt.tmm0]=
    - Where the bit 0 is simply to differentiated between a forwarded pointer and an
      actual lisp object header
    - Where the 2 bits marked =mm= are used by the garbage to collector to mark objects
    - Where the 5 bits marked =tttt t= are used to encode the lisp object type
    - Where the remaining 56 bits encode the number of bytes the object takes up
      - This is the number of bytes AND NOT words as some objects use this to determine a
        "length" vs the garbage collector which only cares about actual size to know how
        to traverse through the object during a garbage collection cycle.
  - The same size a tagged pointer
    #+begin_src C :eval no
      typedef lisp_tagged_pointer lisp_header;
      typedef lisp_header lheader;

      struct lisp_cons;
      typedef struct lisp_cons lcons;
    #+end_src
- Accessible through a union of all of the types (the following shows with objects cons
  and symbols only.)
  #+begin_src C :eval no
    union lisp_object {
            lisp_tagged_pointer self; // for the actual pointer value, useful for EQ
            lobject *p;
            lheader *header; // for accessing only the header of the object
            lcons *cons;
            lsymbol *symbol;
    };
  #+end_src
  - =self= is used when we want the integer value of the pointer. useful for doing pointer
    arithmetic and for testing for object equality (lisp =EQ=).
  - =p= is used for using the lisp_object as a pointer to a lisp object.
  - =header= all immediate objects have a lisp header as its first item. (a simple kind of
    structure inheritance). This allows one to access the header without caring about the
    type of object
  - =cons= used to treat as pointing to a cons, useful when we already know that the
    object is a cons (usually by checking the =header= first).
  - =symbol= like cons, used for treating the pointer as pointing to a symbol. Also used
    when we know that the object is a symbol.
