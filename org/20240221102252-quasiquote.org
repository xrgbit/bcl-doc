#+title: quasiquote
#+roam_tags: backquote qq unquote

Quasiquotation/Backquotation is a facility of many lisps that allows the programmer to
easily create lisp forms out of templates in a syntax that looks exactly like regular lisp
quotation. They are invaluable in creating macros, since they allow the programmer to
easily create lisp forms out of a template and "insert" values into the "holes" of the
template.

[[file:20240221102803-quasiquotation_in_lisp_alan_bawden.org]["Quasiquotation in Lisp" Alan Bawden]] is an excellent paper on the details of quasiquotation and
contains a two implementations of quasiquote
