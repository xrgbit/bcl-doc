#+title: tagged pointer
#+roam_tags: lisp-object immediate-type

An object that fits in the same space as a pointer, whose bits encode how to treat the
pointer. Ie whether that object is to be treated as a pointer, or if it's representing
some other type that takes up the same space as that pointer.

Why would you want a tagged pointer: Some objects don't need to be implemented/realized as
full objects (so called). In the context of this lisp, each object () contains /at a minimum/ a header
field (also the size of a tagged pointer). This would be cumbersome and an inefficient use
of space for objects that normally take up less space than a the width of a pointer:
namely integers and characters. We were to treat them as regular objects, you'd need to
have, at a minimum, the equivalent of 3 tagged pointers. (1) One for the pointer to the
object, (2) another for the header of the object (which encodes, among other things, its
type), and (3) the actual object data.
