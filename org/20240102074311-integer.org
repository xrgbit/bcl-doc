#+title: fixnum
#+roam_tags: lisp-object
A fixnum is an integer that is encoded to fit the same space as a tag pointer
An [[file:20240101112501-immediat_type.org][immediate type]].

* Challenges/Goals
- Create an encoding for fixnum integers that have

#+begin_src C
  #define make_lfixnum(x) ((lobject)(uintptr_t)(((intptr_t)(x) * 2) + 0x1))
  #define lobject_cinteger(x) ((((intptr_t)(x).self - 1) / 2))
#+end_src

- why use =intptr_t=? :: because we want to take advantage of C knowing the right to do
  with negative numbers, so we use the signed integer pointer type to create a /fixnum/
  from a c integer. And when we convert back to a c integer in =lobject_cinteger=, we use
  the int pointer again so that when we divide by 2, if the number was negative it stays
  negative, and if it was positive it stays positive.
- why not use shifts? :: because IIRC, the bit shifts make no guarantee as to how they
  handle shifting values of negative integers, it's implementation dependent (1) that
  numbers are being represented in twos compliment for signed integers and (2) that
  arithmetic right shift will bit extend in the case that the numbers are in twos
  compliment. So =* 2= and =/ 2= it is
- what happens to a value that when multiplied by 2 looks like a negative number? :: well
  one should only be relying on the least significant 63 bits of an integer when creating
  a fixnum. For this early lisp, it's just an error. A more sophisticated approach would
  be to implement [[file:../../../../org/roam/20240105075752-bignum.org][bignums]].
