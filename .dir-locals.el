((nil . ((eval . (setq-local
		  org-roam-directory (expand-file-name (concatenate 'string
								    (locate-dominating-file
								     default-directory ".dir-locals.el")
								    "/org"))))
	 (eval . (setq-local
		  org-roam-db-location (expand-file-name "./org/org-roam.db"
							 org-roam-directory))))))
