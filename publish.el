(let ((bootstrap-file
       (expand-file-name "straight/repos/straight.el/bootstrap.el" user-emacs-directory))
      (bootstrap-version 5))
  (unless (file-exists-p bootstrap-file)
    (with-current-buffer
        (url-retrieve-synchronously
         "https://raw.githubusercontent.com/raxod502/straight.el/develop/install.el"
         'silent 'inhibit-cookies)
      (goto-char (point-max))
      (eval-print-last-sexp)))
  (load bootstrap-file nil 'nomessage))

(setq straight-use-package-by-default t)
(setq org-export-with-tasks nil)

(straight-use-package 'use-package)

(use-package doom-themes
  :config
  (setq doom-themes-enable-bold t    ; if nil, bold is universally disabled
        doom-themes-enable-italic t) ; if nil, italics is universally disabled
  (load-theme 'doom-one t)
  (doom-themes-org-config))

(use-package rainbow-delimiters
  :hook ((scheme-mode lisp-mode emacs-lisp-mode sly-mrepl) . rainbow-delimiters-mode))

(use-package graphviz-dot-mode
  :ensure t
  :config
  (setq graphviz-dot-indent-width 4))

(use-package org-ref)
(require 'ox-publish)
(setq org-html-head-extra
      (concat
       org-html-head-extra
       "\n"
       "<link href=\"https://fonts.googleapis.com/css2?family=Noto+Serif+JP&display=swap\"
       rel=\"stylesheet\">"
       "\n"
       "<link rel=\"stylesheet\" href=\"rgbit-src-style.css\">"
       "<link rel=\"stylesheet\" href=\"rgbit-style.css\">"
       "\n"))

(setq org-html-htmlize-output-type 'css)
(use-package org-roam
    :delight)
(use-package htmlize
  :config
  (progn

    ;; It is required to disable `fci-mode' when `htmlize-buffer' is called;
    ;; otherwise the invisible fci characters show up as funky looking
    ;; visible characters in the source code blocks in the html file.
    ;; http://lists.gnu.org/archive/html/emacs-orgmode/2014-09/msg00777.html
    (with-eval-after-load 'fill-column-indicator
      (defvar modi/htmlize-initial-fci-state nil
        "Variable to store the state of `fci-mode' when `htmlize-buffer' is called.")

      (defun modi/htmlize-before-hook-fci-disable ()
        (setq modi/htmlize-initial-fci-state fci-mode)
        (when fci-mode
          (fci-mode -1)))

      (defun modi/htmlize-after-hook-fci-enable-maybe ()
        (when modi/htmlize-initial-fci-state
          (fci-mode 1)))

      (add-hook 'htmlize-before-hook #'modi/htmlize-before-hook-fci-disable)
      (add-hook 'htmlize-after-hook #'modi/htmlize-after-hook-fci-enable-maybe))

    ;; `flyspell-mode' also has to be disabled because depending on the
    ;; theme, the squiggly underlines can either show up in the html file
    ;; or cause elisp errors like:
    ;; (wrong-type-argument number-or-marker-p (nil . 100))
    (with-eval-after-load 'flyspell
      (defvar modi/htmlize-initial-flyspell-state nil
        "Variable to store the state of `flyspell-mode' when `htmlize-buffer' is called.")

      (defun modi/htmlize-before-hook-flyspell-disable ()
        (setq modi/htmlize-initial-flyspell-state flyspell-mode)
        (when flyspell-mode
          (flyspell-mode -1)))

      (defun modi/htmlize-after-hook-flyspell-enable-maybe ()
        (when modi/htmlize-initial-flyspell-state
          (flyspell-mode 1)))

      (add-hook 'htmlize-before-hook #'modi/htmlize-before-hook-flyspell-disable)
      (add-hook 'htmlize-after-hook #'modi/htmlize-after-hook-flyspell-enable-maybe))))

(org-babel-do-load-languages
 'org-babel-load-languages
 '((shell . t)
   (C . t)
   (emacs-lisp . t)))

;;; we want some of the overrides that org-roam provides for better links
(setq org-publish-project-alist
      '((:author . "Juan-Andres Martinez")
        ("bcl-files"
         :base-directory "./org"
         :base-extension "org"
         :publishing-directory "./html"
         ;; :exclude "\\bhtml/" ; don't want anything that gets placed under html to be searched
         :headline-levels 4
         :recursive t
         :publishing-function org-html-publish-to-html
         :auto-sitemap t)
        ("bcl-static"
         :base-directory "./static"
         :base-extension "css\\|bib"
         :publishing-directory "./html"
         :exclude "\\bhtml/"            ; same as bcl-files
         :recursive t
         :publishing-function org-publish-attachment)
        ("bcl-images"
         :base-directory "./img/"
         :publishing-directory "./html/img"
         :base-extension "svg\\|png"
         :recursive nil
         :publishing-function org-publish-attachment)
        ("bcl" :components ("bcl-files" "bcl-static" "bcl-images")))
      org-confirm-babel-evaluate nil
      org-export-with-section-numbers nil)
